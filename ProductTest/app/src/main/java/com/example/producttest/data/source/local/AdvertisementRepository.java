package com.example.producttest.data.source.local;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.producttest.data.source.local.registered_advertisement.RegisteredAdvertisement;
import com.example.producttest.data.source.local.registered_advertisement.RegistredAdvertisementDao;
import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;
import com.example.producttest.data.source.local.advertisement_id.AdvertisementIdDao;

import java.time.OffsetDateTime;
import java.util.List;

// The repository seperates the DB from the app itself, and adds an abstraction for the DB operations.
public class AdvertisementRepository {

    AdvertisementIdDao advertisementIdDao;
    RegistredAdvertisementDao registredAdvertisementDao;

    private LiveData<List<AdvertisementId>> allAdvertisementIDS;
    private LiveData<List<RegisteredAdvertisement>> allRegisteredAdvertisements;
    private LiveData<List<RegisteredAdvertisement>> registeredSickAdvertisements;

    public AdvertisementRepository(Application application) {
        ProductTestRoomDatabase db = ProductTestRoomDatabase.getDatabase(application);
        advertisementIdDao = db.advertisementIdDao();
        registredAdvertisementDao = db.registredAdvertisementDao();

        // Get a stream of the data, so the stream is updated as the data is updated
        allAdvertisementIDS = advertisementIdDao.getAllIds();
        allRegisteredAdvertisements = registredAdvertisementDao.getAllAdvertisements();
        registeredSickAdvertisements = registredAdvertisementDao.getAllWhereAdvertiserRegisteredSick();
    }

    public LiveData<List<AdvertisementId>> getAllAdvertisementIds() { return allAdvertisementIDS; }
    public LiveData<List<RegisteredAdvertisement>> getAllRegisteredAdvertisements() { return allRegisteredAdvertisements; }
    public LiveData<List<RegisteredAdvertisement>> getAllRegisteredSickAdvertisements() {return registeredSickAdvertisements;}

    // Do operations on the data. Uses the databaseWriteExecutor to ensure that the operation is done on another thread
    public void insertAdvertisementId(final AdvertisementId advertisementId ) {
        ProductTestRoomDatabase.databaseWriteExecutor.execute(() ->
                advertisementIdDao.insert(advertisementId));
    }
    public void insertRegisteredAdvertisement(final RegisteredAdvertisement registeredAdvertisement ) {
        ProductTestRoomDatabase.databaseWriteExecutor.execute(() ->
                registredAdvertisementDao.insert(registeredAdvertisement));
    }

    public void setAdvertiserRegisteredSick(String meetID) {
        ProductTestRoomDatabase.databaseWriteExecutor.execute(() ->
                registredAdvertisementDao.setAdvertiserRegisteredSick(meetID));
    }
    public void deleteAllRegisteredAdvertisements() {
        ProductTestRoomDatabase.databaseWriteExecutor.execute(() ->
                registredAdvertisementDao.deleteAll());
    }
    public void deleteAllAdvertisementIDS() {
        ProductTestRoomDatabase.databaseWriteExecutor.execute(() ->
                advertisementIdDao.deleteAll());
    }
    public void deleteAllOlderThan(int days, int hours) {
        OffsetDateTime time = OffsetDateTime.now().minusDays(days).minusHours(hours);
        ProductTestRoomDatabase.databaseWriteExecutor.execute(()->
                registredAdvertisementDao.deleteOlder(time));
        ProductTestRoomDatabase.databaseWriteExecutor.execute(()->
                advertisementIdDao.deleteOlder(time));
    }
}

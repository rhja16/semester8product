package com.example.producttest.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.producttest.bluetooth.ServiceAdvertisement;
import com.example.producttest.bluetooth.ServiceScanning;
import com.example.producttest.navigation.MainActivity;
import com.example.producttest.R;
import com.example.producttest.data.source.local.AdvertisementRepository;
import com.example.producttest.utilities.Constants;

public class CommunicationService extends Service {
    private final static String TAG = "communication_service";
    // Used to determine wether the service is running, so the UI can toogle the service on/ off.
    public static boolean serviceRunning = false;
    //region Actions
    public static final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    public static final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";
    // endregion

    private ServiceAdvertisement mServiceAdvertisement;
    private ServiceScanning mServiceScanning;

    // Binder to give to clients
    private final IBinder mBinder = new LocalBinder();
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public CommunicationService getService(){
            return CommunicationService.this;
        }
    }


    public int getRandomInt(){
        return 4;
    }
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case ACTION_START_FOREGROUND_SERVICE:
                        // Setup of BT functionality
                        _startCommunicationService();
                        _debugMessage("Foreground service started");
                        break;
                    case ACTION_STOP_FOREGROUND_SERVICE:
                        // This action tear down all the BT functionality of the service
                        // If the service was not running, there would be no BT to tear down.
                        if (serviceRunning) {
                            _tearDownCommunicationService();
                        } else {
                            _debugMessage("Tried tear down but was not running");
                        }
                        // The
                        stopSelf();
                }
            }
        }
        return super.onStartCommand(intent, flags, startID);
    }

    // Make proper service setup
    private void _startCommunicationService() {
        AdvertisementRepository advertisementRepository = new AdvertisementRepository(getApplication());
        // The bluetooth manager is a system service
        // It is an app that communicate with the bluetooth controller.
        // Get the bluetooth adapter
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();

        mServiceAdvertisement = new ServiceAdvertisement(bluetoothAdapter, advertisementRepository);
        mServiceScanning = new ServiceScanning(bluetoothAdapter, advertisementRepository);
        // Check if BT is enabled. If not, make a broadcast reciever that starts the BLE functionality when BT is enabled
        if(!bluetoothAdapter.isEnabled()) {
            _setupNotification("BT er ikke slået til, så registrering er ikke aktiv");
        }
        else {
            _setupNotification("Registrering kører som forventet");
            _startBT();
        }


        IntentFilter intent = new IntentFilter(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
        intent.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(btStateChangedReceiver, intent);


        serviceRunning = true;

    }

    /**
     * Start the BT functionality when the BT is turned on.
     */
    private final BroadcastReceiver btStateChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            _debugMessage("State changed");
            final String action = intent.getAction();
            if(action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_ON) {
                    _startBT();
                    _setupNotification("Registrering kører som forventet");
                }
                if(state == BluetoothAdapter.STATE_OFF){
                    _setupNotification("BT er ikke slået til, så registrering er ikke aktiv");
                }
            }
        }
    };

    private void _startBT() {
        mServiceAdvertisement.StartBLEAdvertising();
        mServiceScanning.StartBLEScan();
    }

    private void _setupNotification(String message) {
        // Notification channel should already be made in application.
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), Constants.MEETING_SYNC_CHANNEL_ID);
        builder.setContentTitle("Smittekæde");
        builder.setContentText(message);
        builder.setSmallIcon(R.drawable.ic_autorenew_black_24dp);
        builder.setContentIntent(pendingIntent);

        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        //builder.setFullScreenIntent(pendingIntent, true);
        Notification notification = builder.build();
        startForeground(1, notification);
    }

    // Stop the BT functionality
    private void _tearDownCommunicationService() {
        _debugMessage("Tear down service");
        mServiceAdvertisement.StopBLEAdvertising();
        mServiceScanning.StopBLEScan();
        serviceRunning = false;
    }
    /**
     * Hjælpefunktion til at debugge for at koden bliver mere ordentlig
     *
     * @param message
     */
    private void _debugMessage(String message) {
        Log.d(TAG, message);
    }





}

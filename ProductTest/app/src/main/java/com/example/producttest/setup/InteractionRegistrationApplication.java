package com.example.producttest.setup;

import android.app.Application;
import android.app.NotificationManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;

import com.example.producttest.service.AdvertiseDataExchangeService;
import com.example.producttest.service.CommunicationService;
import com.example.producttest.service.DeleteIrrelevantDataService;
import com.example.producttest.data.source.local.AdvertisementRepository;
import com.example.producttest.data.source.local.registered_advertisement.RegisteredAdvertisement;
import com.example.producttest.data.source.remote.FirebaseController;
import com.example.producttest.utilities.MessageUtilities;
import com.example.producttest.R;

import java.util.List;

/**
 * Klassen repræsenterer hele appen, og bliver lavet før noget andet.
 */
public class InteractionRegistrationApplication extends Application {
    // Debug string
    private final static String TAG = "Application class";
    // Request kode der bruges til at identificere scan intent hvis den skal fjernes
    // Bliver ikke brugt på nuværende tidspunkt til andet end oprettelsen
    private final static int ScanPendingIntentRequestCode = 0;
    private AdvertisementRepository mAdvertisementRepository;
    /**
     * OnCreate for the application class is called each time the application process is started.
     * This also happens when system is booting,
     * as the manifest specifies that the application should be started when phone is launched
     * This means that the tracking will continue after the phone was reset.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        mAdvertisementRepository = new AdvertisementRepository(this);
        // Notification channel must be created before any notifications are made
        MessageUtilities.CreateNotificationChannel(getString(R.string.channel_name), getString(R.string.channel_description), getSystemService(NotificationManager.class));


        mAdvertisementRepository.getAllRegisteredSickAdvertisements().observeForever((new Observer<List<RegisteredAdvertisement>>() {
            @Override
            public void onChanged(List<RegisteredAdvertisement> registeredAdvertisements) {
                FirebaseController.GetGuideline(registeredAdvertisements.size(), getApplicationContext());
                FirebaseController.GetRegisteredSickText();
            }
        }));
        FirebaseController.GetDescription();
        FirebaseController.ListenForNewDocuments(mAdvertisementRepository);
        // When using Android O and later it is necessary to register a notification channel for the application
        // so notifications are identified with the application

        SetupCommunicationService();
        _setupSceduledDelete();
        // Setup firebase data

    }
    public static final int DeletionJobID = 0;

    // Scedule a job that delete old data every 24 hous
    private void _setupSceduledDelete(){
        JobScheduler jobScheduler = (JobScheduler)getSystemService(Context.JOB_SCHEDULER_SERVICE);

        ComponentName deletionService = new ComponentName(this, DeleteIrrelevantDataService.class);
        JobInfo.Builder builder = new JobInfo.Builder(DeletionJobID, deletionService);
        // Scedule the job to run once every 24 hours. Time is in ms.
        builder.setPeriodic(1000 * 60 * 15);//60*24);
        int res = jobScheduler.schedule(builder.build());
        Log.d(TAG, "Tried sceduling, got result " + res);
    }
    // Start the communication service every time the application process is started,
    // with no need for UI to open
    public void SetupCommunicationService() {
        Intent serviceIntent = new Intent(this, AdvertiseDataExchangeService.class);
        serviceIntent.setAction(AdvertiseDataExchangeService.ACTION_START_FOREGROUND_SERVICE);
        ContextCompat.startForegroundService(this, serviceIntent);
    }
}

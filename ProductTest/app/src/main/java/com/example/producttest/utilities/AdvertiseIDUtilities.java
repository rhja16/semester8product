package com.example.producttest.utilities;

import android.util.Log;

import java.security.SecureRandom;

public class AdvertiseIDUtilities {
    private final static String TAG = "Advertise ID utilities";

    // Convert the bytearray advertiser ID to a string
    public static String ConvertBytearrayToString(byte[] bytes) {
        if(bytes == null) {
            Log.d(TAG, "Called function: ConvertByteArrayToInt, with null array");
            return "";
        }
        String result = "";
        for (int i = 0; i < bytes.length; i++) {
            int j = bytes[i];
            result += j;
        }
        return result;
    }

    // The length of each advertise ID
    public static final int advertiseDataLength = 13;
    // Generates a random byte array with max nr of bytes allowed in advertisement
    // Generated the same way UUID is generated
    public static byte[] GenerateNewAdvertiseID() {
        SecureRandom ng = new SecureRandom();

        // Højest mængde advertise data vi har tilgængelig til service data er 13 byte
        // Gøres denne værdi større, fejler advertisement
        byte[] randomBytes = new byte[advertiseDataLength];
        // Genererer nye koder
        ng.nextBytes(randomBytes);
        return randomBytes;
    }
}

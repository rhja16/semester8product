package com.example.producttest.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.os.ParcelUuid;
import android.util.Log;

import com.example.producttest.data.source.local.AdvertisementRepository;
import com.example.producttest.data.source.local.registered_advertisement.RegisteredAdvertisement;
import com.example.producttest.utilities.AdvertiseIDUtilities;

import java.util.ArrayList;
import java.util.List;

import static com.example.producttest.utilities.Constants.serviceUUID;

public class ServiceScanning {

    private static final String TAG = "Service Scanning";

    public static final String SCANSTARTED = "Started";
    public static final String SCANWaiting = "Waiting";
    public static final String SCANSTOPPED = "Stopped";
    public String status = SCANWaiting;
    private BluetoothAdapter mBluetoothAdapter;
    private AdvertisementRepository mAdvertisementRepository;
    public ServiceScanning(BluetoothAdapter bluetoothAdapter, AdvertisementRepository advertisementRepository){
        mBluetoothAdapter = bluetoothAdapter;
        mAdvertisementRepository = advertisementRepository;
    }
    public void StartBLEScan() {
        status = SCANSTARTED;
        // Lav scan filtre
        ScanFilter scanFilter = (new ScanFilter.Builder()).setServiceUuid(ParcelUuid.fromString(serviceUUID)).build();
        List<ScanFilter> filters = new ArrayList<>();
        filters.add(scanFilter);

        ScanSettings scanSettings = (new ScanSettings.Builder())
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .setNumOfMatches(ScanSettings.MATCH_NUM_ONE_ADVERTISEMENT)
//                .setReportDelay(1000*60*5) // Report delay is put here if desired.
                .setMatchMode(ScanSettings.MATCH_MODE_STICKY)
                .build();
        mBluetoothAdapter.getBluetoothLeScanner().startScan(filters, scanSettings, scanCallback);
    }

    // Stop the BLE scan
    public void StopBLEScan() {
        status = SCANSTOPPED;
        // Make sure that pending scan results are delivered before the scan is stopped
        mBluetoothAdapter.getBluetoothLeScanner().flushPendingScanResults(scanCallback);
        mBluetoothAdapter.getBluetoothLeScanner().stopScan(scanCallback);
    }

    // Callback is made each time a scan result matching the filter is received
    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            _debugMessage("Scan result ");
            super.onScanResult(callbackType, result);
            _addRegisteredAdvertisementToDB(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            _debugMessage("Batch scan result recieved " + results.size());
            for (int i = 0; i < results.size(); i++) {
                _addRegisteredAdvertisementToDB(results.get(i));
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            _debugMessage("Scan failed with error: " + errorCode);
        }


        /**
         * Add a registered advertisement to the database in proper format
         * @param scanResult
         */
        private void _addRegisteredAdvertisementToDB(ScanResult scanResult) {
            ScanRecord scanRecord = scanResult.getScanRecord();
            // Make sure that the scan record is valid so we dont break the system
            if (scanRecord != null) {
                byte[] serviceData = scanRecord.getServiceData(ParcelUuid.fromString(serviceUUID));
                if (serviceData != null) {

                    String serviceDataString = AdvertiseIDUtilities.ConvertBytearrayToString(serviceData);
                    RegisteredAdvertisement registeredAdvertisement = new RegisteredAdvertisement(serviceDataString, scanResult.getRssi(), scanResult.getTxPower());
                    mAdvertisementRepository.insertRegisteredAdvertisement(registeredAdvertisement);
                }
            }
        }
    };

    private void _debugMessage(String message) {
        Log.d(TAG, message);
    }
}

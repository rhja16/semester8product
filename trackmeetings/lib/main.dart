//import 'dart:convert';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/material.dart';
//import 'dart:async';

//import 'package:flutter_p2p/flutter_p2p.dart';
//import 'package:flutter_p2p/gen/protos/protos.pb.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override 
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Test 123",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Testing..."),
        ),
        body: WifiHandler(),
      ),
    );
  }
}

class WifiHandler extends StatefulWidget {
  _WifiHandlerState createState() => _WifiHandlerState();
}
class _WifiHandlerState extends State<WifiHandler> with WidgetsBindingObserver {

  @override
  void initState() {
    super.initState();
    state += "Add observer";
    WidgetsBinding.instance.addObserver(this);
  }
  @override 
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    state += "Remove observer";    
    super.dispose();
  }

  Future<bool> _checkPermission() async {
    print("Testing permissions");
    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.contacts);
    if(false) {
      return false;
    }
    return true;
  }
  bool permissionGranted = false;
  String state = "Begin";
  @override
  Widget build(BuildContext context) {
    _checkPermission();
    print("Building things...");
    return Text('Last notifications: $_notification $permissionGranted');
  }

  AppLifecycleState _notification;

  //Opdater lifecycle state 
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _notification = state;
    });
  }
  
}
package com.example.producttest.utilities;

import android.os.ParcelUuid;

public class Constants {
    public static final String standardID1 = "2f234454-cf6d-4a0f-adf2-f4911ba9ffa6";
    public static final String beaconLayout = "m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25";
    public static final String MEETING_SYNC_CHANNEL_ID = "meetingSyncChannel";
    public static final ParcelUuid myAppUUID = ParcelUuid.fromString("6f8527b7-8bea-4b50-aab0-e1c934d662f6");

    // region UUID konstanter
    public static String serviceUUID = "6f8527b7-8bea-4b50-aab0-e1c934d662f6";
    public static String meetingIDCharacteristicUUID = "c8212161-02fa-4200-bafb-064646c3aec7";
    public static String idAgreementStatusCharacteristicUUID = "c8212161-02fa-4200-bafb-064646c3aec7";
    // endregion
}

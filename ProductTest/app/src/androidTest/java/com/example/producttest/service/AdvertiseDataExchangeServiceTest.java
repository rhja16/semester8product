package com.example.producttest.service;

import android.content.Intent;
import android.os.IBinder;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.MediumTest;
import androidx.test.rule.ServiceTestRule;

import com.example.producttest.bluetooth.ServiceAdvertisement;
import com.example.producttest.bluetooth.ServiceScanning;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Time;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class AdvertiseDataExchangeServiceTest {

    @Rule
    public final ServiceTestRule serviceTestRule = new ServiceTestRule();

    @Test
    public void TestServiceRunningIsUpdatedCorrectly() throws TimeoutException {
        assertFalse(AdvertiseDataExchangeService.serviceRunning);
        getStartedService();
        assertTrue(AdvertiseDataExchangeService.serviceRunning);
        StopService();
        assertFalse(AdvertiseDataExchangeService.serviceRunning);
    }
    @Test
    public void TestServiceCallEnableBTOnStartAndStopBTOnStop() throws TimeoutException {

        AdvertiseDataExchangeService service = getStartedService();
        assertEquals(service.GetAdvStatus(), ServiceAdvertisement.ADVSTARTED);
        assertEquals(service.GetScanStatus(), ServiceScanning.SCANSTARTED);
        StopService();
        assertEquals(service.GetAdvStatus(), ServiceAdvertisement.ADVSTOPPED);
        assertEquals(service.GetScanStatus(), ServiceScanning.SCANSTOPPED);
    }

    Intent serviceIntent;
    private AdvertiseDataExchangeService getStartedService() throws TimeoutException{
        serviceIntent =
                new Intent(getApplicationContext(), AdvertiseDataExchangeService.class);
        serviceIntent.setAction(AdvertiseDataExchangeService.ACTION_START_FOREGROUND_SERVICE);

        IBinder binder = serviceTestRule.bindService(serviceIntent);

        AdvertiseDataExchangeService service = ((AdvertiseDataExchangeService.LocalBinder) binder).getService();

        serviceTestRule.startService(serviceIntent);
        return service;
    }
    private void StopService() throws TimeoutException{
        serviceIntent.setAction(AdvertiseDataExchangeService.ACTION_STOP_FOREGROUND_SERVICE);
        serviceTestRule.startService(serviceIntent);
    }
}
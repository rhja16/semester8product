package com.example.producttest.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.os.ParcelUuid;
import android.util.Log;

import com.example.producttest.data.source.local.AdvertisementRepository;
import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;
import com.example.producttest.utilities.AdvertiseIDUtilities;

import static com.example.producttest.utilities.Constants.serviceUUID;

public class ServiceAdvertisement {
    private static final String TAG = "ServiceAdvertisement";

    public static final String ADVSTARTED = "Started";
    public static final String ADVWaiting = "Waiting";
    public static final String ADVSTOPPED = "Stopped";
    public String status = ADVWaiting;

    //region BLE advertising
    private byte[] currentAdvertiseID;
    private BluetoothAdapter mBluetoothAdapter;
    private AdvertisementRepository mAdvertisementRepository;
    public ServiceAdvertisement(BluetoothAdapter bluetoothAdapter, AdvertisementRepository advertisementRepository) {
        mBluetoothAdapter = bluetoothAdapter;
        mAdvertisementRepository = advertisementRepository;
    }
    // Callback for advertise begin
    private AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            super.onStartSuccess(settingsInEffect);
            Log.d(TAG, "Advertisement start sucess");
            mAdvertisementRepository.insertAdvertisementId(new AdvertisementId(AdvertiseIDUtilities.ConvertBytearrayToString(currentAdvertiseID)));
        }

        @Override
        public void onStartFailure(int errorCode) {
            super.onStartFailure(errorCode);

            Log.d(TAG, "Advertise start failure " + errorCode);
        }
    };

    /**
     * Begin advertisement with BLE
     * Add advertisement package for service with unique id
     */
    public void StartBLEAdvertising() {
        status = ADVSTARTED;
        AdvertiseSettings advertiseSettings = new AdvertiseSettings.Builder()
                // There is no connectable functionality of this service
                .setConnectable(false)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_ULTRA_LOW)
                .build();

        // Make advertisement include the UUID of this service
        AdvertiseData advertiseData = new AdvertiseData.Builder()
                .addServiceUuid(ParcelUuid.fromString(serviceUUID))
                .setIncludeTxPowerLevel(true)
                .build();

        currentAdvertiseID = AdvertiseIDUtilities.GenerateNewAdvertiseID();
        // The scanresponse is sent by default on android smartphones
        // Scanresponse let us add more data to the advertisement
        AdvertiseData scanResponse = new AdvertiseData.Builder()
                .addServiceData(ParcelUuid.fromString(serviceUUID), currentAdvertiseID)
                .build();

        // Start the advertisement
        mBluetoothAdapter.getBluetoothLeAdvertiser().startAdvertising(advertiseSettings, advertiseData, scanResponse, mAdvertiseCallback);
    }
    // Stop the advertisement of the service
    public void StopBLEAdvertising() {
        status = ADVSTOPPED;
        // The bluetooth adapter stops advertisement with the callback
        mBluetoothAdapter.getBluetoothLeAdvertiser().stopAdvertising(mAdvertiseCallback);
    }
}

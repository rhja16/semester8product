package com.example.producttest.data.source.remote;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.example.producttest.data.source.local.AdvertisementRepository;
import com.example.producttest.data.source.local.registered_advertisement.RegisteredAdvertisement;
import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;
import com.example.producttest.utilities.MessageUtilities;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FirebaseController {

    public final static String guidelineCollectionName = "Retningslinjer";
    public final static String guidelineMessageName = "message";
    public final static String guidelinePriorityName = "priority";
    public final static String guidelineThresholdName = "threshold";
    public final static String guidelineWarnName = "warn";
    public final static String guidelineColorName = "color";
    public final static String guidelineHeaderName = "header";

    public final static String descriptionRelevansPeriodName = "relevansperiod";
    public final static String descriptionRelevansDaysName = "days";
    public final static String descriptionRelevansHoursName = "hours";
    public final static String descriptionCollName = "description";
    public final static String descriptionDocName = "text";
    public final static String descriptionContentFieldName = "textcontent";
    public final static String advertiseIDFieldName = "AdvID";
    public final static String timestampFieldName = "timestamp";
    public final static String advIDCollectionPathName = "advertiseIDs";
    public final static String registeredSickTextName = "registeredSick";

    public static MutableLiveData<String> iconColor = new MutableLiveData<String>("#C7E6EC") {};
    public static MutableLiveData<String> guideline = new MutableLiveData<String>("Venter på svar...") {};
    public static MutableLiveData<String> header = new MutableLiveData<String>("Henter status"){};
    public static String description = "Venter på indhold";
    public static String registeredSickText = "Du har registreret dig syg";

    private String _formatAdvertiseTime(RegisteredAdvertisement registeredAdvertisement) {
        return registeredAdvertisement.getTimestamp().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    private final static FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    private final static String TAG = "FirebaseController";

    public static void uploadIds(List<AdvertisementId> advertisementIds) {
        WriteBatch batch = firestore.batch();
        for (int i = 0; i < advertisementIds.size(); i++) {
            Map<String, Object> advertisementID = new HashMap<>();
            advertisementID.put(advertiseIDFieldName, advertisementIds.get(i).getID());
            advertisementID.put(timestampFieldName, advertisementIds.get(i).getBroadcastTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

            DocumentReference advIDDoc = firestore.collection(advIDCollectionPathName).document();
            batch.set(advIDDoc, advertisementID);
        }
        batch.commit();
    }

    // Get guideline message depending on the advertisements where the advertiser registered sick.
    public static String getGuideline(List<RegisteredAdvertisement> advertisements) {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // Threshold should be smaller than actual number.
        // Should get the smallest
        CollectionReference collectionRef = db.collection(FirebaseController.guidelineCollectionName);
        collectionRef
                .whereLessThanOrEqualTo(guidelineThresholdName, advertisements.size())
                .orderBy(guidelineThresholdName, Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                String data = document.getData().get(guidelineMessageName).toString();
                                Log.d(TAG, "Got guideline message " + data);
                            }
                        }
                    }
                });
        return "";
    }

    public static void GetGuideline(int size, Context context) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // Threshold should be smaller than actual number.
        // Should get the smallest
        // TODO move back in controller
        CollectionReference collectionRef = db.collection(FirebaseController.guidelineCollectionName);
        collectionRef
                .whereLessThanOrEqualTo(FirebaseController.guidelineThresholdName, size)
                .orderBy(FirebaseController.guidelineThresholdName, Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                String existingGuideline = guideline.getValue();
                                String newGuideline = document.getData().get(guidelineMessageName).toString();
                                if (!existingGuideline.equals(newGuideline)) {
                                    guideline.setValue(newGuideline);
                                    iconColor.setValue(document.getData().get(guidelineColorName).toString());
                                    header.setValue(document.getData().get(guidelineHeaderName).toString());
                                    String warncode = document.getData().get(FirebaseController.guidelineWarnName).toString();
                                    if (warncode.equals("1")) {
                                        MessageUtilities.MakeNotification(context, "Advarsel om smitterisiko", newGuideline, 10);
                                    }
                                }
                            }
                        }
                    }
                });
    }
    /**
     * Listen for meetings and get notified when something changes
     * Looks through all documents at start up
     */
    public static void ListenForNewDocuments(AdvertisementRepository advertisementRepository) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference collectionRef = db.collection(FirebaseController.advIDCollectionPathName);
        collectionRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    return;
                }
                for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                    if (dc.getType() == DocumentChange.Type.ADDED) {
                        if (dc.getDocument().getData().containsKey(FirebaseController.advertiseIDFieldName)) {
                            // Opdater mødet hvis det er i listen
                            // TODO Find ud af om der bør laves et if exists check. Det virker umiddelbart fint uden.
                            advertisementRepository.setAdvertiserRegisteredSick(dc.getDocument().get(FirebaseController.advertiseIDFieldName).toString());
                        } else {
                            Log.d(TAG, "No ID: " + dc.getDocument().getData().toString());
                        }
                        // Når nye dokumenter bliver tilføjet skal der undersøges i den lokale DB denne enhed har været med
                    }
                }
            }
        });
    }

    public static void GetRegisteredSickText(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection(FirebaseController.descriptionCollName).document(FirebaseController.registeredSickTextName);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        registeredSickText = document.getData().get(FirebaseController.descriptionDocName).toString();

                    }
                }
            }
        });

    }

    public static void GetDescription() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection(FirebaseController.descriptionCollName).document(FirebaseController.descriptionDocName);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if(document.exists()) {
                        description = document.getData().get(FirebaseController.descriptionContentFieldName).toString();
                    }
                    else {
                        description = "Noget gik galt og beskrivelse kunne ikke fines. Prøv igen senere";
                    }
                }
                else {
                    description = "Beskrivelsen kunne desværre ikke hentes. Prøv igen senere";
                }
            }
        });


    }
}



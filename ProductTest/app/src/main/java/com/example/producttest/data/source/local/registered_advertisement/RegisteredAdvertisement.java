package com.example.producttest.data.source.local.registered_advertisement;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

// Repræsenterer at en adverisement er set fra en anden telefon der har appen kørende
@Entity(tableName = "registeredAdvertisement")
public class RegisteredAdvertisement {

    // There must  always be an advertiserID, a timestamp and the signal strength for the advertisement to be valid
    public RegisteredAdvertisement(String advertiserID, int rssi, int txpower) {
        this.advertiserID = advertiserID;
        this.timestamp = OffsetDateTime.now();
        this.rssi = rssi;
        // By default I assume that the advertiser has not registered sick when the advertisement is seen.
        advertiserRegisteredSick = false;
        this.txpower = txpower;
    }
    //Because none of my fields are unique, as the granularity of the timestamp can make several advertisements seem at the same time, I have a ID field
    @PrimaryKey(autoGenerate = true)
    int dbID;
    public int getDbID() {
        return dbID;
    }

    @NonNull
    int txpower;
    public int getTxpower() {return txpower;}

    @NonNull
    // ID der bliver broadcastet af den anden enhed
            String advertiserID;

    @NonNull
    public String getAdvertiserID() {
        return advertiserID;
    }

    // Tidspunktet hvor advertisement blev set.
    @NonNull
    OffsetDateTime timestamp;
    @NonNull
    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    @NonNull
    // Signalstyrken for advertisement. Denne indikerer hvor langt væk den anden person var
    int rssi;
    public int getRssi() {
        return rssi;
    }

    @NonNull
    // Indikerer om den person der lavede advertisement har meldt sig syg og lagt data på serveren
    // Room DBMS does not support boolean, so I have implemented a converter that convert between int and boolean
    // In the DB, true is 1 and false is 0. Room framework takes care of all details in the conversion
    public boolean advertiserRegisteredSick;
}

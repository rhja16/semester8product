package com.example.producttest.data.source.local.advertisement_id;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;

import java.time.OffsetDateTime;
import java.util.List;

@Dao
public interface AdvertisementIdDao {
    // Insert an adverisement in the DB
    @Insert
    void insert(AdvertisementId id);

    // Get all data saved in advertisement class
    @Query("SELECT * FROM advertisementid")
    LiveData<List<AdvertisementId>> getAllIds();

    // Deleta all data in the advertisementid table
    // Enables the user to chose themselves if they want to keep data.
    @Query("DELETE FROM advertisementid")
    void deleteAll();

    @Query("DELETE FROM advertisementid WHERE broadcastTime < :dateTime")
    void deleteOlder(OffsetDateTime dateTime);
    // TODO Make a delete query that deletes everything before a specific date.
}

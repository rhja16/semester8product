package com.example.producttest;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleInstrumentedTest {

    public class A {
        public int B() {return 1;}
    }
    @Mock
    A bta;


    @Test
    public void useAppContext() {

        verify(bta).B();
    }
}

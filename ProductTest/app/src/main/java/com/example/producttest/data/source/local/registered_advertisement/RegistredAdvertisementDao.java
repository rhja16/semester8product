package com.example.producttest.data.source.local.registered_advertisement;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


import java.time.OffsetDateTime;
import java.util.List;
// Object that control the access to the registered advertisements in the room database
@Dao
public interface RegistredAdvertisementDao {

    // Insert a registered advertisement in the room database
    @Insert
    void insert(RegisteredAdvertisement advertisement);

    // Update all advertisements with given advertisementID so advertiserregisteredsick is set to true
    // As ROOM does not support boolean, the converter specifies that true is 1.
    @Query("UPDATE registeredAdvertisement SET advertiserRegisteredSick = 1 WHERE advertiserID = :advertiseID ")
    void setAdvertiserRegisteredSick(String advertiseID);

    // Get all data saved in advertisement class
    @Query("SELECT * FROM registeredAdvertisement")
    LiveData<List<RegisteredAdvertisement>> getAllAdvertisements();

    // Get all instances where the advertiser is registered sick
    @Query("SELECT * FROM registeredAdvertisement WHERE advertiserRegisteredSick = 1")
    LiveData<List<RegisteredAdvertisement>> getAllWhereAdvertiserRegisteredSick();

    // Delete all data from the database.
    // Is used to enable the user to controle their data
    @Query("DELETE FROM registeredAdvertisement")
    void deleteAll();

    @Query("DELETE FROM registeredAdvertisement WHERE timestamp < :dateTime")
    void deleteOlder(OffsetDateTime dateTime);
    // TODO add a delete method that delete all content before a specific date
}


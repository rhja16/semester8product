package com.example.producttest.data.source.local.advertisement_id;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.OffsetDateTime;

// Represents an advertisement ID that the user of this device has advertised
@Entity(tableName = "advertisementid")
public class AdvertisementId {
    @PrimaryKey
    @NonNull
    String ID;
    public String getID() {return ID; }

    // The time that the id was used for broadcasting
    @NonNull
    OffsetDateTime broadcastTime;
    public OffsetDateTime getBroadcastTime() { return broadcastTime; }
    public AdvertisementId(String ID) {
        this.ID = ID;
        this.broadcastTime = OffsetDateTime.now();
    }
}

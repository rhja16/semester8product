package com.example.producttest.showdata;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.producttest.data.source.local.AdvertisementRepository;
import com.example.producttest.data.source.local.registered_advertisement.RegisteredAdvertisement;


import java.util.List;

public class RegisteredAdvertisementViewModel extends AndroidViewModel {

    private AdvertisementRepository mAdvertisementRepository;

    private LiveData<List<RegisteredAdvertisement>> mAllRegisteredAdvertisements;
    private LiveData<List<RegisteredAdvertisement>> mAllRegisteredSickAdvertisements;

    public RegisteredAdvertisementViewModel(Application application) {
        super(application);

        mAdvertisementRepository = new AdvertisementRepository(application);
        mAllRegisteredSickAdvertisements = mAdvertisementRepository.getAllRegisteredSickAdvertisements();
        mAllRegisteredAdvertisements = mAdvertisementRepository.getAllRegisteredAdvertisements();
    }

    public LiveData<List<RegisteredAdvertisement>> getmAllRegisteredAdvertisements() {
        return mAllRegisteredAdvertisements;
    }
    public LiveData<List<RegisteredAdvertisement>> getmRegisteredSickAdvertisements(){
        return mAllRegisteredSickAdvertisements;
    }

    public void insert(RegisteredAdvertisement registeredAdvertisement) {
        mAdvertisementRepository.insertRegisteredAdvertisement(registeredAdvertisement);
    }

    public void deleteAllRegisteredAdvertisements() {
        mAdvertisementRepository.deleteAllRegisteredAdvertisements();
    }
}


package com.example.producttest.showdata;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.producttest.R;
import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class AdvertisementIdListAdapter extends RecyclerView.Adapter<AdvertisementIdListAdapter.AdvertisementIdViewHolder> {

    /**
     * Bliver kaldt når en viewholder laves.
     * Laver et itemView af den rigtige item type, og sender det til viewholder constructoren
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public AdvertisementIdViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.advertisement_id_item, parent, false);
        return new AdvertisementIdListAdapter.AdvertisementIdViewHolder(itemView);
    }

    // Is called one time for each element in the list.
    // The holder is connected to a layout item, so one layout item is genereated for each item in the list.
    @Override
    public void onBindViewHolder(@NonNull AdvertisementIdListAdapter.AdvertisementIdViewHolder holder, int position) {
        if (mAdvertisementIds != null) {
            // Get the RegisteredAdvertisement at current index.
            AdvertisementId current = mAdvertisementIds.get(position);
            holder.advertiseIDView.setText(current.getID());
            holder.timeView.setText(_formatAdvertiseTime(current));

        } else {
            holder.advertiseIDView.setText("----");
            holder.timeView.setText("----");
        }
    }
    private String _formatAdvertiseTime(AdvertisementId advertisementId) {
        return advertisementId.getBroadcastTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    @Override
    public int getItemCount() {
        if (mAdvertisementIds != null) {
            return mAdvertisementIds.size();
        }
        return 0;
    }

    /**
     *
     * @param advertisementIds
     */
    public void setAdvertisementIds(List<AdvertisementId> advertisementIds) {
        mAdvertisementIds = advertisementIds;
        notifyDataSetChanged();
    }

    /**
     * En klasse lavet til at holde informationer om en enkelt beaconOcurrence.
     * Fungerer ud fra textview typen "beaconocurrence", og binder dens relevante felter til views
     */
    class AdvertisementIdViewHolder extends RecyclerView.ViewHolder {
        private final TextView advertiseIDView;
        private final TextView timeView;

        private AdvertisementIdViewHolder(View itemView) {
            super(itemView);
            // Sætter reference til de to relevante views i repræsentation.
            advertiseIDView = itemView.findViewById(R.id.advertiseID);
            timeView = itemView.findViewById(R.id.timestamp);
        }
    }


    private final LayoutInflater mInflater;
    // Cached kopi af vores beaconItems fra DB.
    // Bliver opdateret ved brug af observer i activity
    private List<AdvertisementId> mAdvertisementIds;

    /**
     * Constructor der sætter inflateren ud fra context.
     *
     * @param context
     */
    public AdvertisementIdListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }
}

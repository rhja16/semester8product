package com.example.producttest.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.producttest.data.source.remote.FirebaseController;
import com.example.producttest.data.source.local.AdvertisementRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

// Job service that delete all entries older than the max time set in DB
// Deletes up to a granularity of ours
public class DeleteIrrelevantDataService extends JobService {
    public static final String TAG = "Delete irrelevant data service";

    @Override
    public boolean onStartJob(JobParameters params) {

        AdvertisementRepository repository = new AdvertisementRepository(getApplication());
        FirebaseFirestore.getInstance().collection(FirebaseController.descriptionCollName).document(FirebaseController.descriptionRelevansPeriodName).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(!task.isSuccessful()) {
                    Log.d(TAG, "Deltion period get failed");
                }
                else {
                    int deletionIntervalDays = Integer.parseInt(task.getResult().getData().get(FirebaseController.descriptionRelevansDaysName).toString());
                    int deletionIntervalHours = Integer.parseInt(task.getResult().getData().get(FirebaseController.descriptionRelevansHoursName).toString());
                    Log.d(TAG, "Delete period is " + deletionIntervalDays + " days, and " + deletionIntervalHours + " hours.");
                    repository.deleteAllOlderThan(deletionIntervalDays, deletionIntervalDays);
                }
            }
        });
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "Scheduled job not running");
        return false;
    }
}

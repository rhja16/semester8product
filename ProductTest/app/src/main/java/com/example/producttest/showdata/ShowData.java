package com.example.producttest.showdata;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.producttest.data.source.local.registered_advertisement.RegisteredAdvertisement;
import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;
import com.example.producttest.databinding.FragmentShowDataBinding;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShowData#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShowData extends Fragment {
    private static final String TAG = "Show data fragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FragmentShowDataBinding binding;
    private RegisteredAdvertisementViewModel mRegisteredAdvertisementViewModel;
    private AdvertisementIdViewModel mAdvertisementIdViewModel;

    public ShowData() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShowData.
     */
    // TODO: Rename and change types and number of parameters
    public static ShowData newInstance(String param1, String param2) {
        ShowData fragment = new ShowData();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    // Create the UI
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout file. Converts the layout file into actual views
        binding = FragmentShowDataBinding.inflate(inflater, container, false);
        _setupAdvertiseIdRecyclerView();
        _setupRegisteredAdvertisementsRecyclerView();
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    private void _setupRegisteredAdvertisementsRecyclerView() {
        RecyclerView recyclerView = binding.registeredAdvertisementRecyclerview;
        // Får view model gennem provider. Dette sørger for at den samme viemodel kal leve videre selvom activity er stoppet
        mRegisteredAdvertisementViewModel = new ViewModelProvider(requireActivity()).get(RegisteredAdvertisementViewModel.class);
        // Lav recycler view

        //final MeetingListAdapter adapter = new MeetingListAdapter(requireActivity());
        final RegisteredAdvertisementListAdapter adapter = new RegisteredAdvertisementListAdapter(requireActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));

        // Vis alle smittede møder i view.
        mRegisteredAdvertisementViewModel.getmAllRegisteredAdvertisements().observe(getActivity(), new Observer<List<RegisteredAdvertisement>>() {
            @Override
            public void onChanged(List<RegisteredAdvertisement> registeredAdvertisements) {
                adapter.setRegisteredAdvertisements(registeredAdvertisements);
            }
        });
    }
    private void _setupAdvertiseIdRecyclerView() {
        RecyclerView recyclerView = binding.advertiseIdRecyclerview;
        // Får view model gennem provider. Dette sørger for at den samme viemodel kal leve videre selvom activity er stoppet
        mAdvertisementIdViewModel = new ViewModelProvider(requireActivity()).get(AdvertisementIdViewModel.class);

        //final MeetingListAdapter adapter = new MeetingListAdapter(requireActivity());
        final AdvertisementIdListAdapter adapter = new AdvertisementIdListAdapter(requireActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));

        // Vis alle smittede møder i view.
        mAdvertisementIdViewModel.getmAllAdvertisementIds().observe(getActivity(), new Observer<List<AdvertisementId>>() {
            @Override
            public void onChanged(List<AdvertisementId> registeredAdvertisements) {
                adapter.setAdvertisementIds(registeredAdvertisements);
            }
        });
    }

}

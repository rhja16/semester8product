package com.example.producttest.home;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;

import androidx.core.widget.ImageViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;
import com.example.producttest.showdata.AdvertisementIdViewModel;
import com.example.producttest.data.source.remote.FirebaseController;
import com.example.producttest.databinding.FragmentHomeScreenBinding;
import com.example.producttest.utilities.MessageUtilities;

import java.util.List;
import java.util.concurrent.Callable;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeScreen extends Fragment {
    private static final String TAG = "Home screen fragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentHomeScreenBinding binding;
    public HomeScreen() {
        // Required empty public constructor

    }

    //FragmentHomeScreenBinding binding;
    private void setupFirestore() {

        //Intent intent = new Intent(this, StoredDataActivity.class);
        //startActivity(intent);
        //listenForNewDocuments();
        // Sætter metode for at registrere sig syg
        // Alle mødeID skal sendes til serveren
        //binding.registrerSyg.setOnClickListener(new View.OnClickListener(){
        //    public void onClick(View v) {
        //        _uploadMeetings();
        //    }
        //});
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeScreen.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeScreen newInstance(String param1, String param2) {
        HomeScreen fragment = new HomeScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    AdvertisementIdViewModel advertisementIdViewModel;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        advertisementIdViewModel = new ViewModelProvider(this).get(AdvertisementIdViewModel.class);

        setupFirestore();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeScreenBinding.inflate(inflater, container, false);
        binding.registrerSyg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageUtilities.MakeAlertDialog(
                        v.getContext(),
                        "Registrer dig smittet",
                        "Er du sikker på at du vil registrere dig smittet",
                        new Callable<Void>() {
                            public Void call() {
                                _uploadMeetings();
                                binding.statusHeader.setVisibility(View.GONE);
                                binding.guidelineMessage.setVisibility(View.GONE);
                                binding.registrerSyg.setVisibility(View.GONE);
                                binding.registeredSickBox.setText(Html.fromHtml(FirebaseController.registeredSickText, 1,null, null));
                                binding.registeredSickBox.setVisibility(View.VISIBLE);

                                return null;
                            }
                        });
            }
        });
        _getGuideline();
        //binding = FragmentHomeScreenBinding.inflate(getLayoutInflater());
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    //region meeting management

    private boolean registeredSickThisSession = false;

    private void _getGuideline() {
        FirebaseController.iconColor.observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.statusHeader.setBackgroundColor(Color.parseColor(s));
                //ImageViewCompat.setImageTintList(binding.statusHeader, ColorStateList.valueOf(Color.parseColor(s)));
            }
        });
        FirebaseController.guideline.observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.guidelineMessage.setText(s);
            }
        });
        FirebaseController.header.observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.statusHeader.setText(s);
            }
        });
    }
    // Bruges til at uploade alle møder når en person er syg
    // Før møder bliver uploaded bør foreground service deaktiveres.
    // Dette er fordi man er smittet og nu ikke længere være aktiv da det kan virke som overvågning.
    private void _uploadMeetings(){
        advertisementIdViewModel.getmAllAdvertisementIds().observe(this, new Observer<List<AdvertisementId>>() {
            @Override
            public void onChanged(List<AdvertisementId> advertisementIds) {
                FirebaseController.uploadIds(advertisementIds);
                advertisementIdViewModel.getmAllAdvertisementIds().removeObserver(this);
            }
        });
    }
    //endregion
}

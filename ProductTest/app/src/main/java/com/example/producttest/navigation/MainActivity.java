package com.example.producttest.navigation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.example.producttest.R;
import com.example.producttest.service.AdvertiseDataExchangeService;
import com.example.producttest.service.CommunicationService;
import com.example.producttest.utilities.MessageUtilities;
import com.example.producttest.utilities.PermissionUtilities;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.concurrent.Callable;

// Beacon consumer interface kan bruges med en aktivitet, service eller et fragment
// Interfacet bruges i samarbejde med BeaconManager, og har et callback til når BeaconService er klar til brug.
public class MainActivity extends AppCompatActivity {

    private NavController navController;
    private AppBarConfiguration appBarConfiguration;

    // Binding klasse for mainActivity der indeholder alle views fra root i den associcerede activity
    private static final String TAG = "Mainactivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _makeToolbarAndNavigationFunctionality();
        permissionUtilities = new PermissionUtilities(this);
        permissionUtilities.GetPermissions();
    }

    private void _makeToolbarAndNavigationFunctionality() {

        _setupToolbarAsActionbar();

        NavHostFragment host = (NavHostFragment)getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        navController = host.getNavController();
        AppBarConfiguration.Builder appBarConfBuilder =  new AppBarConfiguration.Builder(navController.getGraph());
        appBarConfiguration = appBarConfBuilder.build();
        setupTopBarNavMenu(navController);
        setupBottomNavMenu(navController);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Toast.makeText(this, "Options item selected", Toast.LENGTH_SHORT).show();
        if(item.getItemId() == R.id.data_page) {
            navController.navigate(R.id.data_page);
            return true;
        }
        return false;
    }

    /**
     * Define homescreen as start screen.
     * Set up page change when button is clicked.
     * @param navController The application navigation controller to link navigation
     */
    private void setupBottomNavMenu(NavController navController) {
        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        // Navigation begins on homescreen
        bottomNav.setSelectedItemId(R.id.home_page);
        //Set up navigation functionality so bottom navigation is linked with the nav controller
        // All functionality is dependend on that the menu items are named after the fragment they lead to in the navigation graph file.
        NavigationUI.setupWithNavController(bottomNav, navController);

    }
    private void setupTopBarNavMenu(NavController navController) {
        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);

        NavigationUI.setupWithNavController(toolbar, navController);
    }

    /**
     * Setup the toolbar as the actionbar, so the framework knows that toolbar is the intented action bar.
     */
    private void _setupToolbarAsActionbar() {
        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        // Make my custom toolbar an action bar
        setSupportActionBar(toolbar);
    }
    // Menu ikoner skal inclates
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        _setupOnOffButton(menu);
        return true;
    }
    Switch sw;
    // Make the service start and stop by toggeling the on off button
    private void _setupOnOffButton(Menu menu) {
        MenuItem item = menu.findItem(R.id.app_bar_on_off);
        RelativeLayout relativeLayout = (RelativeLayout) item.getActionView();

        sw = (Switch)relativeLayout.getChildAt(0);
        sw.setChecked(AdvertiseDataExchangeService.serviceRunning);
        // Turn off the service only works until the app is started again or the phone is restarted.
        // TODO make the service app turn off until it is turned on agin by the user. Make a popup the next time the app is opened if it is not tracking!
        sw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AdvertiseDataExchangeService.serviceRunning) {
                    MessageUtilities.MakeAlertDialog(
                            v.getContext(),
                            getString(R.string.turn_off_title),
                            getString(R.string.turn_off_message),
                            new Callable<Void>() {
                                public Void call() {
                                    Intent serviceIntent = new Intent(getApplicationContext(), AdvertiseDataExchangeService.class);
                                    serviceIntent.setAction(AdvertiseDataExchangeService.ACTION_STOP_FOREGROUND_SERVICE);
                                    // Stop the service by calling start service with stop action instead of stop service.
                                    // This is so the service can close down properly
                                    getApplicationContext().startService(serviceIntent);
                                    sw.setChecked(false);
                                    return null;
                                }
                    });
                    sw.setChecked(AdvertiseDataExchangeService.serviceRunning);
                }
                else {
                    Intent serviceIntent = new Intent(getApplicationContext(), AdvertiseDataExchangeService.class);
                    serviceIntent.setAction(AdvertiseDataExchangeService.ACTION_START_FOREGROUND_SERVICE);
                    ContextCompat.startForegroundService(getApplicationContext(), serviceIntent);
                    sw.setChecked(true);
                }
            }
        });
    }

    PermissionUtilities permissionUtilities;
    @Override
    public void onRequestPermissionsResult(int requestcode, String permissions[], int[] grantResults){
        switch (requestcode){
            case PermissionUtilities.PERMISSION_REQUEST_BACKGROUND_LOCATION: {

            }
            // If we did not have these permissions on app startup, the scan will fail.
                // Therefore wes start the scan on request granted
            case PermissionUtilities.PERMISSION_REQUEST_FINE_LOCATION: {
                Intent serviceIntent = new Intent(getApplicationContext(), AdvertiseDataExchangeService.class);
                serviceIntent.setAction(AdvertiseDataExchangeService.ACTION_PERMISSION_GRANTED_START_SCAN);
                ContextCompat.startForegroundService(getApplicationContext(), serviceIntent);
                sw.setChecked(true);
            }
            case PermissionUtilities.PERMISSION_REQUEST_COARSE_LOCATION: {
                Intent serviceIntent = new Intent(getApplicationContext(), AdvertiseDataExchangeService.class);
                serviceIntent.setAction(AdvertiseDataExchangeService.ACTION_PERMISSION_GRANTED_START_SCAN);
                ContextCompat.startForegroundService(getApplicationContext(), serviceIntent);
                sw.setChecked(true);
            }
            case PermissionUtilities.PERMISSION_REQUEST_BLUETOOTH: {

            }
            case PermissionUtilities.PERMISSION_REQUEST_BLUETOOTH_ADMIN: {

            }
        }
    }



}

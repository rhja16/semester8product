package com.example.producttest.utilities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.producttest.R;
import com.example.producttest.utilities.Constants;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.concurrent.Callable;

/**
 * Utility class for sending messages to the user.
 */
public class MessageUtilities {
    private static final String TAG = "messageutil";

    /**
     * Display Alert dialog for the user, with a positive button action.
     * @param context the context the dialog should be shown in.
     * @param title The dialog title. Should be kept short.
     * @param message The dialog message. Should describe the consequenses of the choise.
     * @param acceptCall A action that is done when user press positive button.
     */
    public static void MakeAlertDialog(Context context, String title, String message, Callable<Void> acceptCall) {

        new MaterialAlertDialogBuilder(context, R.style.AlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ja, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            acceptCall.call();
                        }
                        catch (Exception e) {
                            Log.d(TAG, "Call failed with message " + e.getMessage());
                        }
                    }
                })
                .setNegativeButton(R.string.nej, null)
                .create().show();
    }

    public static void ShowDialogBox(Context context, String title, String message) {
        new MaterialAlertDialogBuilder(context, R.style.AlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", null)
                .create().show();
    }
    /**
     * Make a notification channel so the system can identify which process notifications are send from
     * Notification channel is included in Android O(API 26+).
     * The method make the build check.
     */
    public static void CreateNotificationChannel(String channelName, String channelDescription, NotificationManager notificationManager) {
        // Make sure channel is only created on build version API 26+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = channelName;
            String description = channelDescription;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(Constants.MEETING_SYNC_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // The channel is registered and cannot be changed after this
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     * Display notification for user.
     * @param context Application context of the notification
     * @param title Notfication title
     * @param text Text content of the notification
     * @param notificationId is the ID of the notification. A new notification on a channel overwrites other notifications with same ID on same channel
     */
    public static void MakeNotification(Context context, String title, String text, int notificationId) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Constants.MEETING_SYNC_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_autorenew_black_24dp)
                .setContentTitle(title)
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationId, builder.build());
    }
}

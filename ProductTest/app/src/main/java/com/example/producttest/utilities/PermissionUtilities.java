package com.example.producttest.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.producttest.BuildConfig;
import com.example.producttest.service.CommunicationService;

public class PermissionUtilities {

    private static final String TAG = "Permission utility";
    private Activity mActivity;

    public PermissionUtilities(Activity activity){
        mActivity = activity;
    }
    //region Permissions
    /**
     * Hjælpefunktioner til at finde ud af om permission er givet.
     * Skal reducere mængden af kode.
     * @return
     */
    // region Permission konstanter
    public static final int PERMISSION_REQUEST_FINE_LOCATION = 10;
    public static final int PERMISSION_REQUEST_COARSE_LOCATION = 11;
    public static final int PERMISSION_REQUEST_BACKGROUND_LOCATION = 12;
    public static final int PERMISSION_REQUEST_BLUETOOTH = 13;
    public static final int PERMISSION_REQUEST_BLUETOOTH_ADMIN = 14;
    // endregion
    public boolean _fineLocationPermissionGranted() {
        boolean finelocation = mActivity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        return finelocation;
    }
    public boolean _coarseLocationPermissionGranted() {
        boolean coarseLocation = mActivity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        return  coarseLocation;
    }
    public boolean _bluetoothPermissionGranted() {
        boolean bluetooth = mActivity.checkSelfPermission(Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED;
        return bluetooth;
    }
    public boolean _bluetoothAdminPermissionGranted() {
        boolean bluetoothAdmin = mActivity.checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED;
        return bluetoothAdmin;
    }
    public boolean _backgroundLocationPermissionGranted() {
        boolean backgroundLocation = mActivity.checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;
        return backgroundLocation;
    }
    //TODO Lav bedre permission interface
    // Et godt permission interface er vigtigt for at brugeren har tillid til appen
    public void GetPermissions() {
        boolean versionMOrGreater = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
        // Håndter permissions på en enhed der har Android M eller nyere
        if(versionMOrGreater) {
            Log.d(TAG, "Android version >=M");
            // Vi skal bruge fine location
            if(_fineLocationPermissionGranted()) {
                Log.d(TAG, "Fine location permission granted");
                // Hvis vi har fine location men ikke background location skal vi bede om det. Hvis vi ikke har fine har vi heller ikke background
                if(_backgroundLocationPermissionGranted()) {
                    Log.d(TAG, "Background permission granted");
                }
                else {
                    Log.d(TAG, "Request bacground permission");
                    ActivityCompat.requestPermissions(
                            mActivity,
                            new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                            PERMISSION_REQUEST_BACKGROUND_LOCATION);
                }
            }
            // I dette tilfælde skal vi have både background og fine location
            else {
                Log.d(TAG, "Requesting  fine and background location");
                ActivityCompat.requestPermissions(
                        mActivity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                        PERMISSION_REQUEST_FINE_LOCATION);

            }
        }
        else {
            Log.d(TAG, "Version <M ");
            if(!_coarseLocationPermissionGranted()) {
                ActivityCompat.requestPermissions(
                        mActivity,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_REQUEST_COARSE_LOCATION
                );
            }
        }
        if(!_bluetoothAdminPermissionGranted()) {
            ActivityCompat.requestPermissions(
                    mActivity,
                    new String[]{Manifest.permission.BLUETOOTH_ADMIN},
                    PERMISSION_REQUEST_BLUETOOTH_ADMIN
            );
        }
        if(!_bluetoothAdminPermissionGranted()) {
            ActivityCompat.requestPermissions(
                    mActivity,
                    new String[]{Manifest.permission.BLUETOOTH},
                    PERMISSION_REQUEST_BLUETOOTH
            );
        }
    }

}

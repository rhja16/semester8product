package com.example.producttest.showdata;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.producttest.R;
import com.example.producttest.data.source.local.registered_advertisement.RegisteredAdvertisement;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class RegisteredAdvertisementListAdapter extends RecyclerView.Adapter<RegisteredAdvertisementListAdapter.RegisteredAdvertisementViewHolder> {

    /**
     * Bliver kaldt når en viewholder laves.
     * Laver et itemView af den rigtige item type, og sender det til viewholder constructoren
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public RegisteredAdvertisementViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.registered_advertisement_item, parent, false);
        return new RegisteredAdvertisementViewHolder(itemView);
    }

    // Is called one time for each element in the list.
    // The holder is connected to a layout item, so one layout item is genereated for each item in the list.
    @Override
    public void onBindViewHolder(@NonNull RegisteredAdvertisementViewHolder holder, int position) {
        if (mRegisteredAdvertisements != null) {
            // Get the RegisteredAdvertisement at current index.
            RegisteredAdvertisement current = mRegisteredAdvertisements.get(position);
            holder.advertiseIDView.setText(current.getAdvertiserID());
            holder.timeView.setText(_formatAdvertiseTime(current));
            holder.rssiView.setText(current.getRssi() + "");
            if(current.advertiserRegisteredSick) {
                holder.registeredSickView.setText("Ja");
            }
            else {
                holder.registeredSickView.setText("Nej");
            }
        } else {
            holder.advertiseIDView.setText("----");
            holder.timeView.setText("----");
            holder.rssiView.setText("-----");
            holder.registeredSickView.setText("----");
        }
    }
    private String _formatAdvertiseTime(RegisteredAdvertisement registeredAdvertisement) {
        return registeredAdvertisement.getTimestamp().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    @Override
    public int getItemCount() {
        if (mRegisteredAdvertisements != null) {
            return mRegisteredAdvertisements.size();
        }
        return 0;
    }

    /**
     * Opdaterer beaconocurences og sørger for at view bliver opdateret.
     *
     * @param registeredAdvertisements
     */
    public void setRegisteredAdvertisements(List<RegisteredAdvertisement> registeredAdvertisements) {
        mRegisteredAdvertisements = registeredAdvertisements;
        notifyDataSetChanged();
    }

    /**
     * En klasse lavet til at holde informationer om en enkelt beaconOcurrence.
     * Fungerer ud fra textview typen "beaconocurrence", og binder dens relevante felter til views
     */
    class RegisteredAdvertisementViewHolder extends RecyclerView.ViewHolder {
        private final TextView advertiseIDView;
        private final TextView rssiView;
        private final TextView timeView;
        private final TextView registeredSickView;

        private RegisteredAdvertisementViewHolder(View itemView) {
            super(itemView);
            // Sætter reference til de to relevante views i repræsentation.
            advertiseIDView = itemView.findViewById(R.id.advertiseID);
            timeView = itemView.findViewById(R.id.timestamp);
            rssiView = itemView.findViewById(R.id.rssi);
            registeredSickView = itemView.findViewById(R.id.advertiser_registered_sick);
        }
    }


    private final LayoutInflater mInflater;
    // Cached kopi af vores beaconItems fra DB.
    // Bliver opdateret ved brug af observer i activity
    private List<RegisteredAdvertisement> mRegisteredAdvertisements;

    /**
     * Constructor der sætter inflateren ud fra context.
     *
     * @param context
     */
    public RegisteredAdvertisementListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

}

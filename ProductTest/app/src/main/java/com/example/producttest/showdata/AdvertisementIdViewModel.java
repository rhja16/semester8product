package com.example.producttest.showdata;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.producttest.data.source.local.AdvertisementRepository;
import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;

import java.util.List;

public class AdvertisementIdViewModel extends AndroidViewModel {

    private AdvertisementRepository mAdvertisementRepository;

    public LiveData<List<AdvertisementId>> mAllAdvertisementIds;

    public AdvertisementIdViewModel(Application application) {
        super(application);

        mAdvertisementRepository = new AdvertisementRepository(application);

        mAllAdvertisementIds = mAdvertisementRepository.getAllAdvertisementIds();
    }

    public LiveData<List<AdvertisementId>> getmAllAdvertisementIds() {
        return mAllAdvertisementIds;
    }

    public void insert(AdvertisementId advertisementId) {
        mAdvertisementRepository.insertAdvertisementId(advertisementId);
    }

    public void deleteAllAdvertisementIds() {
        mAdvertisementRepository.deleteAllAdvertisementIDS();
    }
}


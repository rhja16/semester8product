package com.example.producttest.utilities;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class AdvertiseIDUtilitiesTest {

    // The advertiseIDUtilities class offer two functions.
    // Each are listed with the functionality that should be verified
    //      GenerateNewAdvertiseID
    //          Generates random, unique IDs
    //          Generates IDs of the required length
    //          Generated array is not null
    //      ConvertByteArrayToString
    //          Converts the bytes to the proper number representation

    @Test
    public void TestThatAdvertiseIDIsProperLength() {
        byte[] bytes = AdvertiseIDUtilities.GenerateNewAdvertiseID();

        assertEquals(bytes.length, AdvertiseIDUtilities.advertiseDataLength);
    }

    @Test
    public void TestThatGeneratedIDsAreUnique_TestWith100() {
        List<byte[]> bytesList = new ArrayList<>();
        boolean duplicate = false;
        for(int i = 0; i < 1000; i++) {
            byte[] bytes = AdvertiseIDUtilities.GenerateNewAdvertiseID();
            for(int j = 0; j < bytesList.size(); j++) {
                if(Arrays.equals(bytesList.get(j),bytes)){
                    duplicate = true;
                }
            }
            bytesList.add(bytes);
        }
        assertFalse(duplicate);
    }
    @Test
    public void TestByteConversionToString_ManyDifferentCharacters() {
        // Given
        String s = "vfk wøæø'ø'scd.æcø.øæ pn ";
        byte[] bytes = s.getBytes();
        String res = "";
        for (int i = 0; i < bytes.length; i++) {
            res += bytes[i];
        }
        // When
        String convertedBack = AdvertiseIDUtilities.ConvertBytearrayToString(bytes);
        // Then
        assertEquals(res, convertedBack);
    }
}
package com.example.producttest.data.source.local;

import androidx.room.TypeConverter;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

// Converter enable me to use datatypes for my entity class that is not supported by the room DBMS
// Converts the datatype to a supported type
public class Converters {

    /**
     * En formatter der sørger for at det altid bliver formateret korrekt
     */
    private DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;


    /**
     * Parser String værdien til en OffSetDateTime ved hjælp af parseren
     * @param value
     * @return
     */
    @TypeConverter
    public OffsetDateTime toOffsetDateTime(String value) {
        return formatter.parse(value, OffsetDateTime::from);
    }

    /**
     * Formaterer date objektet til en streng for samme repræsentation som før.
     * @param date
     * @return
     */
    @TypeConverter
    public String fromOffsetDateTime(OffsetDateTime date) {
        return date.format(formatter);
    }


    // Convert between boolean and int. I prefer to use boolean in the entity class, as it is more intuitive
    @TypeConverter
    public int fromBoolean(boolean value)
    {
        if(value){ return 1; }
        else {return 0;}
    }
    @TypeConverter
    public boolean fromInt(int value)
    {
        if(value == 1) {
            return true;
        }
        else {
            return false;
        }
    }
}

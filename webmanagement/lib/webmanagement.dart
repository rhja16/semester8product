/*
Denne fil fungerer som API for min plugin pakke. 
Formålet med dette plugin er at lave platform specifik kode der håndterer WIFI managing for enheder.

*/

import 'dart:async';

import 'package:flutter/services.dart';

class Webmanagement {
  static const MethodChannel _channel =
      const MethodChannel('webmanagement');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}

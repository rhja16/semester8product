/*
import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_p2p/flutter_p2p.dart';
import 'package:flutter_p2p/gen/protos/protos.pb.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    _register();
    print('InitState');
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    print('Dispose');
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('Did change app lifecycle');
    if (state == AppLifecycleState.resumed) {
      print('Resumed state, register');
      _register();
    } else if (state == AppLifecycleState.paused) {
      print('Paused state, unregister');
      _unregister();
    }
  }
  
  List<WifiP2pDevice> devices = [];

  var _isConnected = false;
  var _isHost = false;

  List<StreamSubscription> _subscriptions = [];
  
  void _register() async {
    if (!await _checkPermission()) {
      return;
    }
    _subscriptions.add(FlutterP2p.wifiEvents.stateChange.listen((change) {
      print("stateChange: ${change.isEnabled}");
    }));

    _subscriptions.add(FlutterP2p.wifiEvents.connectionChange.listen((change) {
      setState(() {
        _isConnected = change.networkInfo.isConnected;
        _isHost = change.wifiP2pInfo.isGroupOwner;
        _deviceAddress = change.wifiP2pInfo.groupOwnerAddress;
      });
      print(
          "connectionChange: ${change.wifiP2pInfo.isGroupOwner}, Connected: ${change.networkInfo.isConnected}");
    }));

    _subscriptions.add(FlutterP2p.wifiEvents.thisDeviceChange.listen((change) {
      print(
          "deviceChange: ${change.deviceName} / ${change.deviceAddress} / ${change.primaryDeviceType} / ${change.secondaryDeviceType} ${change.isGroupOwner ? 'GO' : '-GO'}");
    }));

    _subscriptions.add(FlutterP2p.wifiEvents.discoveryChange.listen((change) {
      print("discoveryStateChange: ${change.isDiscovering}");
    }));

    _subscriptions.add(FlutterP2p.wifiEvents.peersChange.listen((change) {
      print("peersChange: ${change.devices.length}");
      change.devices.forEach((device) {
        print("device: ${device.deviceName} / ${device.deviceAddress}");
      });

      setState(() {
        devices = change.devices;
      });
    }));

    FlutterP2p.register();
  }

  void _unregister() {
    _subscriptions.forEach((subscription) => subscription.cancel());
    FlutterP2p.unregister();
  }

  P2pSocket _socket;
  void _openPortAndAccept(int port) async {
    var socket = await FlutterP2p.openHostPort(port);
    setState(() {
      _socket = socket;
    });

    var buffer = "";
    socket.inputStream.listen((data) {
      var msg = String.fromCharCodes(data.data);
      buffer += msg;
      if (data.dataAvailable == 0) {
        snackBar("Data Received: $buffer");
        socket.writeString("Successfully received: $buffer");
        buffer = "";
      }
    });

    print("_openPort done");

    await FlutterP2p.acceptPort(port);
    print("_accept done");
  }

  var _deviceAddress = "";

  _connectToPort(int port) async {
    var socket = await FlutterP2p.connectToHost(
      _deviceAddress,
      port,
      timeout: 100000,
    );

    setState(() {
      _socket = socket;
    });

    _socket.inputStream.listen((data) {
      var msg = utf8.decode(data.data);
      snackBar("Received from Host: $msg");
    });

    print("_connectToPort done");
  }

  Future<bool> _checkPermission() async {
    if (!await FlutterP2p.isLocationPermissionGranted()) {
      await FlutterP2p.requestLocationPermission();
      return false;
    }
    return true;
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Network testing',
      
      home: Scaffold(
        appBar: AppBar(
          title: Text("Netværk testing"),
        ),
        body: Text("Temp2"),
        /*
        key: _scaffoldKey,
        appBar: AppBar(
          title: const Text('Plugin example app 2'),
        ),
        body: Column(
          children: <Widget>[
            Text(_isConnected
                ? "Connected: ${_isHost ? "Host" : "Client"}"
                : "Disconnected"),
            RaisedButton(
              onPressed: () => FlutterP2p.discoverDevices(),
              child: Text("Discover Devices"),
            ),
            RaisedButton(
              onPressed: _isConnected && _isHost
                  ? () => _openPortAndAccept(8888)
                  : null,
              child: Text("Open and accept data from port 8888"),
            ),
            RaisedButton(
              onPressed: _isConnected ? () => _connectToPort(8888) : null,
              child: Text("Connect to port 8888"),
            ),
            RaisedButton(
              onPressed: _socket != null
                  ? () => _socket.writeString("Hello World")
                  : null,
              child: Text("Send hello world"),
            ),
            RaisedButton(
              onPressed: _isConnected ? () => FlutterP2p.removeGroup() : null,
              child: Text("Disconnect"),
            ),
            Expanded(
              child: ListView(
                children: this.devices.map((d) {
                  return ListTile(
                    title: Text(d.deviceName),
                    subtitle: Text(d.deviceAddress),
                    onTap: () {
                      print(
                          "${_isConnected ? "Disconnect" : "Connect"} to device: $_deviceAddress");
                      return _isConnected
                          ? FlutterP2p.cancelConnect(d)
                          : FlutterP2p.connect(d);
                    },
                  );
                }).toList(),
              ),
            ),
          ],
        ),*/
      ),
    );
  }

  snackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(text),
        duration: Duration(seconds: 2),
      ),
    );
  }
}
/*
import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_p2p/flutter_p2p.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Future<bool> _checkPermission() async {
  if (!await FlutterP2p.isLocationPermissionGranted()) {
    await FlutterP2p.requestLocationPermission();
    return false;
  }
  return true;
}
  @override
  Widget build(BuildContext context) {
    _checkPermission();
    return MaterialApp(
      title: 'Welcome to Flutter',
      theme: ThemeData(
        primaryColor: Colors.blueGrey,
      ),
      home: NetworkMeassure(),
    );
  }
}

class NetworkMeassure extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Netværk mål'),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.account_box),
            title: NetworkData(Connectivity().getWifiIP()),
          ),
          ListTile(
            title: NetworkData(Connectivity().getWifiName()),
          ),
        ],
      ),
    );
  }
}

class NetworkData extends StatelessWidget {
  NetworkData(this.info);
  final Future<String> info;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
      future: info,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text('Ingen netværks information tilgængelig');
        }
        if (snapshot.hasData) {
          return Text(snapshot.data,
              style: Theme.of(context).textTheme.headline);
        } else {
          return Text('Data waiting');
        }
      },
    );
  }
}

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();
}

class RandomWordsState extends State<RandomWords> {
  final List<WordPair> _suggestions = <WordPair>[];
  final Set<WordPair> _saved = Set<WordPair>();
  final TextStyle _biggerFont = const TextStyle(fontSize: 18);

  void _pushSaved() {
    Navigator.of(context)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      final Iterable<ListTile> tiles = _saved.map(
        (WordPair pair) {
          return ListTile(
            title: Text(
              pair.asPascalCase,
              style: _biggerFont,
            ),
          );
        },
      );
      final List<Widget> divided =
          ListTile.divideTiles(tiles: tiles, context: context).toList();
      return Scaffold(
        appBar: AppBar(
          title: Text('Saved Suggestions'),
        ),
        body: ListView(children: divided),
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.list), onPressed: _pushSaved),
        ],
      ),
      body: _buildSuggestions(),
    );
  }

  Widget _buildRow(WordPair pair) {
    final bool alreadySaved = _saved.contains(pair);
    return ListTile(
        title: Text(
          pair.asPascalCase,
          style: _biggerFont,
        ),
        trailing: Icon(
          alreadySaved ? Icons.favorite : Icons.favorite_border,
          color: alreadySaved ? Colors.red : null,
        ),
        onTap: () {
          setState(() {
            if (alreadySaved) {
              _saved.remove(pair);
            } else {
              _saved.add(pair);
            }
          });
        });
  }

  Widget _buildSuggestions() {
    return ListView.builder(
        padding: const EdgeInsets.all(16),
        itemBuilder: (BuildContext _context, int i) {
          if (i.isOdd) {
            return Divider();
          }
          final int index = i ~/ 2;

          if (index >= _suggestions.length) {
            _suggestions.addAll(generateWordPairs().take(10));
          }
          return _buildRow(_suggestions[index]);
        });
  }

}
  connectivity: ^0.4.8+2
  provider: ^3.2.0
*/*/
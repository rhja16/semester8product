package com.example.producttest.data.source.local;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.producttest.data.source.local.registered_advertisement.RegisteredAdvertisement;
import com.example.producttest.data.source.local.registered_advertisement.RegistredAdvertisementDao;
import com.example.producttest.data.source.local.advertisement_id.AdvertisementId;
import com.example.producttest.data.source.local.advertisement_id.AdvertisementIdDao;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// TODO Find ud af det der export schema
@Database(entities = { RegisteredAdvertisement.class, AdvertisementId.class}, version = 1, exportSchema = false)
/**
 * En database der indeholder alle data jeg bruger.
 */
@TypeConverters({Converters.class})
public abstract class ProductTestRoomDatabase extends RoomDatabase{

    public abstract AdvertisementIdDao advertisementIdDao();
    public abstract RegistredAdvertisementDao registredAdvertisementDao();
    /**
     * En instans af databasen. Gør at der altid kun vil være én instans
     */
    private static volatile ProductTestRoomDatabase INSTANCE;

    // TODO Find ud af hvor mange tråde jeg skal have, og hvilken rolle det spiller
    private static final int NUMBER_OF_THREADS = 4;
    // TODO Find ud af hvilken rolle alt det her spiller
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    /**
     * Hent Database instansen. Hvis den ikke allerede findes bliver den bygget her
     * @param context
     * @return
     */
    public static ProductTestRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ProductTestRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Instanse bliver bygget af database builder.
                    // Den bliver koblet til applikations kontexten
                    // Den får defineret sit navn
                    // Den injecter sin klasse definition
                    INSTANCE =
                            Room.databaseBuilder(
                                    context.getApplicationContext(),
                                    ProductTestRoomDatabase.class,
                                    "beacon_occurence_database")
                                    // Her tilføjes et callback for specifik implementation
                                    .addCallback(sRoomDatabaseCallback)
                                    .fallbackToDestructiveMigration()
                                    .build();
                }
            }
        }
        return INSTANCE;
    }
    /**
     * Callback for databasen som bliver brugt til at tilføje custom opførsel
     */
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        }
    };

    //region Database migrations
    /*
    Her er database migrations der definerer måden databasen skal håndtere at gå fra én version til en anden.
    Dette sker på tidspunkter hvor fx skemaer bliver opdateret.

    Databasen har en identiy hash der beskriver hvilken version der bliver kørt.
    Hvis databasen er opdateret, men versionen ikke er, får man en exception.
    Ved at køre en migration opdateres identity hash for databasen.
     */
    /**
     * Migration fra version 1 til version 2 hvor jeg opdaterede meetings schema
     */
    /*
    static final Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            OffsetDateTime offsetDateTime = OffsetDateTime.now();
            String converted = (new Converters()).fromOffsetDateTime(offsetDateTime);
            String falsestring = "false";
            Log.d("Room", "Converted is " + converted);

            //database.execSQL("ALTER TABLE meeting ADD COLUMN isBroadcasted TEXT NOT NULL DEFAULT 'false'");

            database.execSQL("CREATE TABLE new_meeting (" +
                    "meetingID TEXT PRIMARY KEY NOT NULL," +
                    "meetTime TEXT NOT NULL, " +
                    "isBroadcasted TEXT NOT NULL DEFAULT '')");

            database.execSQL("INSERT INTO new_meeting (meetingID, meetTime) " +
                    " SELECT meetingID, '" + converted + "' as meetTime FROM meeting");
            database.execSQL("DROP TABLE meeting");
            database.execSQL("ALTER TABLE new_meeting RENAME TO meeting");

            database.execSQL(
                    "CREATE TABLE meeting_new (" +
                            "meetingID TEXT PRIMARY KEY, " +
                            "meetTime TEXT NOT NULL DEFAULT " + converted + ", " +
                            " isBroadcasted TEXT NOT NULL DEFAULT false)");
            database.execSQL(
                    "INSERT INTO meeting_new (meetingID) SELECT meetingID FROM meeting"
            );
            database.execSQL("DROP TABLE meeting");
            database.execSQL("ALTER TABLE meeting_new RENAME TO meeting");


        }
    };
    /*
     */
    //endregion
}
